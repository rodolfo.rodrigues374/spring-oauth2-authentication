package app.com.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/teste")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class TesteController {

	
	@RequestMapping(value = "/pesquisa", method = RequestMethod.GET)
	public ResponseEntity pesquisaProduto() {
		return new ResponseEntity("RODOLFO", HttpStatus.OK);

	}
}