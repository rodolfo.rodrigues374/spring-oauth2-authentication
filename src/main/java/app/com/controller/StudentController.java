package app.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.com.request.StudentRequest;
import app.com.service.IStudentService;

@RestController
@RequestMapping("/student")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class StudentController {

	@Autowired
	private IStudentService studentService;
	
	@PreAuthorize("hasAuthority('PESQUISAR')")
	@RequestMapping(value = "/pesquisa", method = RequestMethod.GET)
	public ResponseEntity pesquisaProduto() {
 		
		return new ResponseEntity(this.studentService.findAll(), HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('CADASTRAR')")
	@RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
	public ResponseEntity CadastrarProduto( @RequestBody StudentRequest request) {
		this.studentService.save(request);
		return new ResponseEntity( HttpStatus.CREATED);

	}
}
