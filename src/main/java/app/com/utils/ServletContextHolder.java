package app.com.utils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ServletContextHolder {
	
    private static WebApplicationContext context = null;

    public static final <T> T getBean(final Class<T> requiredType) {
    	
		if(context == null) {
			final ServletRequestAttributes requestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			if(requestAttributes != null) {
				final HttpServletRequest request = requestAttributes.getRequest();
				final ServletContext servletContext = request.getServletContext();
	            context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			}
		}
		
		try {
			return (context != null ? context.getBean(requiredType) : requiredType.getConstructor().newInstance());
		}catch(final Throwable throwable) {
			throwable.printStackTrace();
			return null;
		}
    }
}