package app.com.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import app.com.token.CustomTokenEnhancer;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	// token validation secret
	private static final String SECRET_JWT = "string-oauth2-autentication";

	//client credentials
	private static final String CLIENT = "angular";

	//secret credentials
	private static final String SECRET = "admin";

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	@Autowired
	private AuthenticationManager authenticationManager;

	//configuration of client credentials 
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

		String secretEnconding = encoder.encode(SECRET);
		
		clients.inMemory()
				.withClient(CLIENT)
				.secret(secretEnconding)
				.scopes("read", "write")
				.authorizedGrantTypes("password", "refresh_token")
				.accessTokenValiditySeconds(1800) // 30 seconds for expire
				.refreshTokenValiditySeconds(3600 * 24); // 24 hours for expire
	}

	//configure end-points to use token
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
 
		
		//Objeto com com mais detalhamento de tokens
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));
		
		endpoints
		.tokenStore(tokenStore())
		.tokenEnhancer(tokenEnhancerChain)
		
		/*
		 *  Sempre que solicitado será enviado um refresh token para o usuário. Ele não se deslogara.
		 *  Se não setado. O refresh token expira em 24 horas.
		 */
		.reuseRefreshTokens(false)
		.authenticationManager(authenticationManager);
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter acessTokenConverter = new JwtAccessTokenConverter();

		acessTokenConverter.setSigningKey(SECRET_JWT);
		return acessTokenConverter;
	}

	// responsible for storing token
	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}
	
	@Bean
	public TokenEnhancer tokenEnhancer() {
		return new CustomTokenEnhancer();
	}
}