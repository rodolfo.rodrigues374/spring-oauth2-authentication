package app.com.config;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import app.com.model.UserEntity; 

public class UsuarioSistema extends User {

	private static final long serialVersionUID = 6931343521848697704L;

	private UserEntity usuario;

	public UsuarioSistema(UserEntity usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getEmail(), usuario.getPassword(), authorities);
		this.usuario = usuario;
	}

	public UserEntity getUsuario() {
		return usuario;
	}
}
