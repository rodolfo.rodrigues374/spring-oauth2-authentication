package app.com.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StudentRequest {
	private Long codigo;
	private String nome;
	private String sexo;
}
