package app.com.component;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import app.com.model.UserEntity;
import lombok.ToString;

@Component
@ToString
public final class UserAuthenticated {

	private UserEntity user = new UserEntity();

	public UserAuthenticated() {

	}

	public UserAuthenticated(final UserEntity user) {

		if (user != null) {
			BeanUtils.copyProperties(user, this.user, "loggedUser");
		}
	}

	public Long getUserId() {
		return user.getId();
	}

	public String getName() {
		return user.getName();
	}

	public String getEmail() {
		return user.getEmail();
	}

}
