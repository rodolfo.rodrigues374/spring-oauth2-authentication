package app.com.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Student {

	private Long codigo;
	private String nome;
	private String sexo;
	
}
