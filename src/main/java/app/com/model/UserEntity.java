package app.com.model;

import java.util.List;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserEntity {

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public UserEntity() {
		
	}
	
	public UserEntity(Long id, String name, String email, String password, List<Permissao> permission) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = encoder.encode(password);
		this.permission = permission;
	}

	private Long id;
	private String name;
	private String email;
	private String password;
	private List<Permissao> permission;
}