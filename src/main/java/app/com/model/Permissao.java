package app.com.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Permissao {

	public Permissao(Long codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	private Long codigo;
	private String descricao;
}
