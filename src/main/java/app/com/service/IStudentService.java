package app.com.service;

import app.com.request.StudentRequest;

public interface IStudentService {

	String findAll();	
	void save(StudentRequest request);
}
