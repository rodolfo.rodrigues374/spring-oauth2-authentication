package app.com.service;

import app.com.service.imple.AutenticacaoUserDetailService;

 
public interface IService {
	
	 
	default Long getLoggedUserId() {
		return AutenticacaoUserDetailService.getAuthenticated().getUserId();
	}

	default String getLoggedName() {		 
		return AutenticacaoUserDetailService.getAuthenticated().getName();
	}
}
