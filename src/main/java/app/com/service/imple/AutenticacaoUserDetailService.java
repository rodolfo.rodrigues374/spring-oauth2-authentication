package app.com.service.imple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import app.com.component.UserAuthenticated;
import app.com.config.UsuarioSistema;
import app.com.model.Permissao;
import app.com.model.UserEntity;
import app.com.utils.ServletContextHolder;

@Service
public class AutenticacaoUserDetailService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
 
		UserEntity usuario = MOCK(email);
		return new UsuarioSistema(usuario, getPermissoes(usuario));
	}

	private Collection<? extends GrantedAuthority> getPermissoes(UserEntity usuario) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		usuario.getPermission()
				.forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getDescricao().toUpperCase())));
		return authorities;
	}

	private static UserEntity MOCK(String email) {
		UserEntity usuario = null;
		ArrayList<Permissao> lista = new ArrayList<>();

		switch (email) {
		case "rodolfo@com.br":

			lista.add(new Permissao(1L, "PESQUISAR"));
			lista.add(new Permissao(2L, "CADASTRAR"));
			usuario = new UserEntity(1L, "Rodolfo", "rodolfo@com.br", "123456", lista);
			break;

		case "priscila@com.br":

			lista.add(new Permissao(1L, "PESQUISAR"));
			usuario = new UserEntity(1L, "Priscila", "priscila@com.br", "123456", lista);
			break;

		default:
			throw new UsernameNotFoundException("User not found!");
		}

		return usuario;
	}
	
	
	public static final UserAuthenticated getAuthenticated() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return getAuthenticated(authentication);
	}

	public static final UserAuthenticated getAuthenticated(final Authentication authentication) {

		try {

			if (authentication != null) {
				
				final Object principal = authentication.getPrincipal();
				
				if (principal instanceof UserAuthenticated) {
					
					return (UserAuthenticated) principal;
					
				} else if(principal instanceof CharSequence ) {
					 
					final AutenticacaoUserDetailService userServiceBean = ServletContextHolder.getBean(AutenticacaoUserDetailService.class);
					
					if (userServiceBean != null) {
						

						final UserEntity user = MOCK(principal.toString());
						
						if (user != null) {
							
							return new UserAuthenticated(user);
						}
					}
				}
			}
			
		} catch (final Throwable throwable) {
			
			throwable.printStackTrace();
		}
		
		return null;
	}

}