# Project for Spring Boot OAuth2 Authentication

Test project for Spring Boot OAuth2 Authentication.

The project used the following technologies.

  - **Swagger** (Used to document API end-points) 
  - **Java 11**
  - **Spring Boot 2.4.5 - Maven**
  - **Lombok** (Lombok is a Java library focused on productivity and code reduction)
  - **Spring Security** (The authentication and authorization framework for the web application)
  - **Oauth 2** (The authentication and authorization framework for the web application)
  - **JWT** (JSON Web Token is an industry standard (RFC-7519) that defines how to transmit and store JSON objects in a compact and secure way between different applications)
   
  
  
  Swagger: http://localhost:8080/swagger-ui/index.html 